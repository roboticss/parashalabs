#include <iostream>
#include <cmath>

using namespace std;

double Bk(double k);
double arrMax(double *arr, int size);
double arrAvg(double *arr, int size);

int main() {
	
	double a[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	double b[10];
	double c[10];

	for (int k = 1; k <= 9; k++)
		b[k - 1] = Bk(double(k));

	for (int k = 1; k <= 9; k++)
		c[k - 1] = min(a[9 - k], b[k]);

	printf("index\ta\tb\tc\n");
	for (int i = 0; i < 10; i++) {
		printf("%d\t%.2lf\t%.2lf\t%.2lf\n", i, a[i], b[i], c[i]);
	}
	printf("\ndiv\t%.2lf\t%.2lf\t%.2lf\n", arrMax(a, 10) - arrAvg(a, 10), arrMax(b, 10) - arrAvg(b, 10), arrMax(c, 10) - arrAvg(c, 10));
	
	return 0;
}

double Bk(double k) {
    return pow(-1, k) * sqrt((k + 2) * (k + 1));
}

double arrMax(double *arr, int size) {
    int max = arr[0];

    for (int i = 0; i < size; i++)
        if (max < arr[i])
            max = arr[i];

    return max;
}

double arrAvg(double *arr, int size) {
    double sum = 0;

    for (int i = 0; i < size; i++)
        sum += arr[i];

    return sum / double(size);
}
