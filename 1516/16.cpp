#include <iostream>
#include <cmath>
#include <vector>

#include "automobile.h"

using namespace std;

int whatToDo();
void printHeader();
void printAutoRow(automobile autom);
void readFromFile();
void searchByYear();


FILE *file;
vector<automobile> autos;

int main() {
	automobile amobile = { "", "", "", 0, "" };

	if (file = fopen("auto.dat", "rb")) {
		readFromFile();
		
		cout << endl << "---";
		printHeader();
		for (automobile autom : autos)
			printAutoRow(autom);
		cout << "---" << endl;

		while (true)
			if (whatToDo() == 2)
				return 0;

		fclose(file);
	}

	return 0;
}

int whatToDo() {
	cout << endl << "What to do: " << endl;
	cout << "   1) Search by year" << endl;
	cout << "   2) Exit" << endl;
	
	int act;
	
	cout << ": ";
	cin >> act;
	
	if (act == 1)
		searchByYear();
	
	return act;
}

void printHeader() {
	printf("\n%s\t%s\t%s\t%s\t%s\n", "number", "brand", "color", "year", "owner");
}

void printAutoRow(automobile autom) {
	printf("%s\t%s\t%s\t%d\t%s\n", autom.number, autom.brand, autom.color, autom.year, autom.owner);
}

void readFromFile() {
	automobile amobile = {};

	fread(&amobile, sizeof(automobile), 1, file);
	while (!feof(file)) {
		autos.push_back(amobile);
		fread(&amobile, sizeof(automobile), 1, file);
	}
}

void searchByYear() {
	int year;
	
	cout << endl << "Enter a year: ";
	cin >> year;

	cout << "---";
	printHeader();
	for (automobile amobile : autos)
		if (amobile.year == year)
			printAutoRow(amobile);
	cout << "---" << endl;
}
