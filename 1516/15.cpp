#include <iostream>
#include <cmath>
#include <vector>

#include "automobile.h"

using namespace std;

int main() {
	automobile amobile = { "", "", "", 0, "" };

	FILE *file;

	if (file = fopen("auto.dat", "a+b")) {
		char oneMore;
			
		do {
			oneMore = 'y';
			amobile = readAutomobile(amobile);

			fwrite(&amobile, sizeof(amobile), 1, file);
			system("clear");

			cout << "Yet another boot? (y/n) ";
			cin >> oneMore;
		} while (oneMore != 'n' && oneMore != 'N');
		
	
		fread(&amobile, sizeof(automobile), 1, file);
		
		printf("\n%s\t%s\t%s\t%s\t%s\n", "number", "brand", "color", "year", "owner");
		while (!feof(file)) {
			printf("%s\t%s\t%s\t%d\t%s\n", amobile.number, amobile.brand, amobile.color, amobile.year, amobile.owner);
			fread(&amobile, sizeof(automobile), 1, file);
		}

		fclose(file);
	}

	return 0;
}

automobile readAutomobile(automobile a) {
	cout << "Number: ";
	cin >> a.number;

	cout << "Brand: ";
	cin >> a.brand;

	cout << "Color: ";
	cin >> a.color;

	cout << "Year: ";
	cin >> a.year;

	cout << "Owner: ";
	cin >> a.owner;

	return a;
}

