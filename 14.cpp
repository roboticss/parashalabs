#include <iostream>
#include <cmath>
#include <vector>

using namespace std;


double absDiv(int *a, int size) {
	int sumUnder = 0;
	int sumUpper = 0;

	for (int i = 0; i < size; i++)
		if (a[i] >= 0)
			sumUpper += a[i];
		else
			sumUnder += a[i];

	return fabs(double(sumUnder) / double(sumUpper));
}

float absDivVec(vector<int> a) {
	int sumUnder = 0;
    int sumUpper = 0;

    for (int i = 0; i < a.size(); i++)
        if (a[i] >= 0)
            sumUpper += a[i];
        else
            sumUnder += a[i];
	
	return fabs(double(sumUnder) / double(sumUpper));
}

int main() {
	
	int aN;
	cout << "Size of A: ";
	cin >> aN;
    
	int a[aN];
    for (int i = 0; i < aN; i++)
		cin >> a[i];


	int bN;
	cout << "Size of B: ";
	cin >> bN;

	cout << "Rand range: " << endl;
	int rangeStart;
	int rangeEnd;
	cin >> rangeStart >> rangeEnd;
	
	int b[bN];
	for (int i = 0; i < bN; i++)
		b[i] = rand() % (rangeEnd - rangeStart + 1) + rangeStart;
	
	vector<int> c;

	for (int i = 0; i < min(aN, bN); i++)
		if (a[i] > b[i])
			c.push_back(a[i]);

	printf("\nindex\ta\tb\tc\n");
    for (int i = 0; i < max(aN, bN); i++) {
		int aI = aN > i ? a[i] : 0;
		int bI = bN > i ? b[i] : 0;

		int cII = 0;
		int cI = c.size() > i ? c[cII++] : 0;
        printf("%d\t%d\t%d\t%d\n", i, aI, bI, cI);
    }
	printf("absDiv\t%.2lf\t%.2lf\t%.2lf\n", absDiv(a, aN), absDiv(b, bN), absDivVec(c));

	return 0;
}
