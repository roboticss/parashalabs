#include <iostream>
#include <cmath>

using namespace std;


class NumbersPair {

protected:
	int first, second;

public:
	NumbersPair();

	NumbersPair(int first, int second);

	void input();

	void print();

	int getFirst() {
		return first;
	}

	int getSecond() {
		return second;
	}

	int sum() {
		return this->first + this->second;
	}

	bool operator==(NumbersPair npair) {
		return npair.getFirst() == this->first && npair.getSecond() == this->second;
	}

	bool operator!=(NumbersPair npair) {
		return !(*this == npair);
	}
};

NumbersPair::NumbersPair() {
	this->first = 0;
	this->second = 0;
}

NumbersPair::NumbersPair(int first, int second) {
	this->first = first;
	this->second = second;
}

void NumbersPair::input() {
	cout << "Enter a NumberPair\n";
	cout << "first: ";
	cin >> first;

	cout << "second: ";
	cin >> second;

	cout << "-- -- --" << endl;
}

void NumbersPair::print() {
	printf("NumbersPair(%d, %d)\n", this->first, this->second);
}

class Vector : public NumbersPair {

protected:
	int x, y;

public:
	Vector();

	Vector(int x, int y);

	void input() {
		cout << "Enter a Vector\n";
		cout << "x: ";
		cin >> x;

		cout << "y: ";
		cin >> y;

		cout << "-- -- --" << endl;
	}

	void print() {
		printf("Vector(%d, %d)\n", this->first, this->second);
	}

	int getX() {
		return x;
	}

	int getY() {
		return y;
	}

	Vector operator+(Vector vec) {
		return {this->x + vec.getX(), this->y + vec.getY()};
	}

	Vector operator-(Vector vec) {
		return {this->x - vec.getX(), this->y - vec.getY()};
	}

	int operator*(Vector vec) {
		return (this->x * vec.getX()) + (this->y * vec.getY());
	}
};

Vector::Vector() {
	this->x = 0;
	this->y = 0;
}

Vector::Vector(int x, int y) {
	this->x = x;
	this->y = y;
}

int main() {
	NumbersPair pair1;
	NumbersPair pair2;

	pair1.input();
	pair2.input();


	Vector vec1;
	Vector vec2;

	vec1.input();
	vec2.input();

	cout << endl << endl;

	cout << "pair1.sum():\t" << pair1.sum() << endl;
	cout << "pair2.sum():\t" << pair2.sum() << endl;
	cout << "pair1 == pair2:\t" << (pair1 == pair2) << endl;
	cout << "pair1 != pair2:\t" << (pair1 != pair2) << endl;

	cout << endl;

	cout << "vec1 + vec2:\t";
	(vec1 + vec2).print();
	cout << "vec1 - vec2:\t";
	(vec1 - vec2).print();
	cout << "vec1 * vec2:\t" << (vec1 * vec2);

	return 0;
}