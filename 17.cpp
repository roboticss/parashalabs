#include <iostream>
#include <cmath>

using namespace std;


class NumbersPair {

protected:
	int x, y;

public:
	NumbersPair();

	NumbersPair(int x, int y);

	int getX() {
		return x;
	}

	int getY() {
		return y;
	}

	string toString() {
		return to_string(this->x) + ", " + to_string(this->y);
	}

	void print() {
		printf("%d\t%d\n", this->x, this->y);
	}

	bool operator==(NumbersPair npair) {
		return npair.getX() == this->x && npair.getY() == this->y;
	}

	bool operator!=(NumbersPair npair) {
		return !(*this == npair);
	}
};

NumbersPair::NumbersPair() {
	this->x = 0;
	this->y = 0;
}

NumbersPair::NumbersPair(int x, int y) {
	this->x = x;
	this->y = y;
}

class Vector : public NumbersPair {

public:
	Vector();

	Vector(int x, int y);


	Vector operator+(Vector vec) {
		return {this->x + vec.getX(), this->y + vec.getY()};
	}

	Vector operator-(Vector vec) {
		return {this->x - vec.getX(), this->y - vec.getY()};
	}

	int operator*(Vector vec) {
		return (this->x * vec.getX()) + (this->y * vec.getY());
	}
};

Vector::Vector() {
	this->x = 0;
	this->y = 0;
}

Vector::Vector(int x, int y) {
	this->x = x;
	this->y = y;
}

int main() {
	NumbersPair pair1 = NumbersPair(10, 20);
	NumbersPair pair2 = NumbersPair(20, 10);

	Vector vec1 = Vector(1, 2);
	Vector vec2 = Vector(2, 3);

	cout << "pair1 == pair2:\t" << (pair1 == pair2) << endl;
	cout << "pair1 != pair2:\t" << (pair1 != pair2) << endl;

	cout << endl;

	cout << "vec1 + vec2:\tVector(" << (vec1 + vec2).toString() << ")" << endl;
	cout << "vec1 - vec2:\tVector(" << (vec1 - vec2).toString() << ")" << endl;
	cout << "vec1 * vec2:\t" << (vec1 * vec2);

	return 0;
}